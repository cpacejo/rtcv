#ifndef CTP_RTCV_H
#define CTP_RTCV_H

#include <atomic>
#include <cerrno>
#include <exception>
extern "C"
{
#include <pthread.h>
#include <signal.h>
}

namespace ctp
{

// Notes:
// notify_all()/notify_one() is async-signal-safe.
// No need to hold lock when calling them.
// Only one thread allowed to wait() at a time.
// Any writes issued prior to notify_all()/notify_one() will
// be visible once wait() returns.

class rtcv
{
public:
    static void initialize(int signum = SIGUSR1);

    void notify_all() noexcept
    {
        // ensure waiter is visible
        if (m_has_waiter.load(std::memory_order_acquire))
        {
            // ensure user operations are visible
            std::atomic_thread_fence(std::memory_order_release);

            if (pthread_kill(m_waiter, m_signum) != 0) std::terminate();
        }
    }

    void notify_one() noexcept { notify_all(); }

    template<typename Pred> void wait(Pred pred)
    {
        if (pred()) return;

        sigset_t sigset;
        if (sigemptyset(&sigset) != 0) std::terminate();
        if (sigaddset(&sigset, m_signum) != 0) std::terminate();

        sigset_t origset;
        if (pthread_sigmask(SIG_BLOCK, &sigset, &origset) != 0) std::terminate();

        m_waiter = pthread_self();
        // ensure waiter is visible
        m_has_waiter.store(true, std::memory_order_release);

        // ensure user operations are visible
        while (std::atomic_thread_fence(std::memory_order_acquire), !pred())
        {
            int sig;
            do { if (sigwait(&sigset, &sig) != 0) std::terminate(); }
            while (sig != m_signum);
        }

        m_has_waiter.store(false, std::memory_order_release);

        if (pthread_sigmask(SIG_UNBLOCK, &sigset, nullptr) != 0) std::terminate();
    }

    // TODO: wait_for/wait_until... but macOS doesn't support sigtimedwait???

private:
    static int m_signum;

    std::atomic<bool> m_has_waiter{false};
    pthread_t m_waiter;
};

}

#endif
