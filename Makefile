CXXFLAGS = -std=c++14 -Wall -Wextra -Werror -O2

OBJS = rtcv.o

rtcv.a: $(OBJS)
	$(AR) rcs $@ $(OBJS)

rtcv.o: rtcv.h
