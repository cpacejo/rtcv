#include <cerrno>
#include <exception>
#include <system_error>
extern "C"
{
#include <signal.h>
}
#include "rtcv.h"

namespace ctp
{

namespace
{

// we need a handler so that the signal is not ignored
void dummy_handler(int) { }

}

int rtcv::m_signum = -1;

void rtcv::initialize(const int signum)
{
    m_signum = signum;

    struct sigaction sa{};
    sa.sa_handler = dummy_handler;
    if (sigaction(m_signum, &sa, nullptr) != 0)
        throw std::system_error(errno, std::generic_category(), "sigaction");
}

}
